package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/gmr458/golang-sql/config"
	"github.com/go-sql-driver/mysql"
)

var db *sql.DB

var envFileName string = ".env"

type Album struct {
	ID     int64
	Title  string
	Artist string
	Price  float32
}

func albumsByArtist(name string) ([]Album, error) {
	// An albums slice to hold data from returned rows.
	var albums []Album

	rows, err := db.Query("SELECT * FROM album WHERE artist = ?", name)

	if err != nil {
		return nil, fmt.Errorf("albumsByArtist %q: %v", name, err)
	}

	defer rows.Close()

	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var alb Album
		if err := rows.Scan(&alb.ID, &alb.Title, &alb.Artist, &alb.Price); err != nil {
			return nil, fmt.Errorf("albumsByArtist %q: %v", name, err)
		}
		albums = append(albums, alb)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("albumsByArtist %q: %v", name, err)
	}

	return albums, nil
}

func albumsByID(id int64) (Album, error) {
	// An album to hold data from the returned row.
	var alb Album

	row := db.QueryRow("SELECT * FROM album WHERE id = ?", id)

	fmt.Println(row)

	if err := row.Scan(&alb.ID, &alb.Title, &alb.Artist, &alb.Price); err != nil {
		if err == sql.ErrNoRows {
			return alb, fmt.Errorf("albumsByID %d: no such album", id)
		}
		return alb, fmt.Errorf("albumsByID %d: %v", id, err)
	}

	return alb, nil
}

func addAlbum(alb Album) (int64, error) {
	result, err := db.Exec("INSERT INTO album (title, artist, price) VALUES (?, ?, ?)", alb.Title, alb.Artist, alb.Price)

	if err != nil {
		return 0, fmt.Errorf("addAlbum: %v", err)
	}

	id, err := result.LastInsertId()

	if err != nil {
		return 0, fmt.Errorf("addAlbum: %v", err)
	}

	return id, nil
}

func main() {
	// Capture connection properties.
	cfg := mysql.Config{
		User:   config.GetEnvVar("DBUSER", envFileName),
		Passwd: config.GetEnvVar("DBPASS", envFileName),
		Net:    "tcp",
		Addr:   config.GetEnvVar("DBADDR", envFileName),
		DBName: config.GetEnvVar("DBNAME", envFileName),
	}

	// Get a database handle.
	var err error
	db, err = sql.Open("mysql", cfg.FormatDSN())

	if err != nil {
		panic(err)
	}

	pingErr := db.Ping()

	if pingErr != nil {
		log.Fatal(pingErr)
	}

	fmt.Println("Connected!")

	/* albums, err := albumsByArtist("John Coltrane")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Albums found: %v\n", albums) */

	/* alb, err := albumsByID(2)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Album found: %v\n", alb) */

	alb := Album{Title: "Happier Than Ever", Artist: "Billie Eilish", Price: 70.99}

	albID, err := addAlbum(alb)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("ID of added album: %v\n", albID)
	}
}
