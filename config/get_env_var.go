package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

func GetEnvVar(key string, envFileName string) string {
	err := godotenv.Load(envFileName)

	if err != nil {
		log.Fatalf("Error loading %s file", envFileName)
	}

	return os.Getenv(key)
}
